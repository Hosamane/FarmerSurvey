from rest_framework import serializers
from farmerSurvey.models import *
from rest_framework.serializers import *
from django.contrib.auth.models import User
from django.db.models import Q

# class UserLoginSerializer(ModelSerializer):
# 	token = CharField(allow_blank =True,read_only=True)
# 	username = CharField()
# 	class Meta:
# 		model = User
# 		fields =[
# 			'username',
# 			'password',
# 			'token'
# 			]
# 		extra_kwargs ={"password":
# 							{"write_only":True}
# 							}
# 	def validate(self,data):
# 		user_obj = None
# 		username = data.get("username")
# 		password = data["password"]
# 		user = User.objects.filter(
# 				Q(username=username)
# 				).distinct()
# 		if user.exists() and user.count() ==1:
# 			user_obj = user.first()
# 		else:
# 			raise ValidationError('This user is not valid')
# 		if user_obj:
# 			if not user_obj.check_password(password):
# 				raise ValidationError("incorrect password")
# 		return data



# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('url', 'username','is_staff')

class InstitutionLog(ModelSerializer):
	class Meta:
		model = Institution
	    # fields = ['pk','Institution_name','address','phone_no']


class MemberSerializer(ModelSerializer):
	
	class Meta:
		model = Member
		# fields = ['name','address','phonenumber','is_preferred','institution']

	
class CropsLog(ModelSerializer):
	
	class Meta:
		model = Crop
		fields = ['pk','crop_name']

class Water_sourceLog(ModelSerializer):
	class Meta:
		model = Water_source
		fields = ['pk','water_source']

class VillageLog(ModelSerializer):
	class Meta:
		model = Village
		fields = ['pk','village']

class FarmerDetailsLog(ModelSerializer):
	
	class Meta:
		model = FarmerDetail
		fields = ['pk','name','mobile_num','district']

class FarmerFieldsLog(ModelSerializer):
	farmerDetails=FarmerDetailsLog(read_only=True)
	crop_name = CropsLog(read_only=True)
	class Meta:
		model = FarmerField
		fields = ['pk','acars','town','water_source','farmerDetails','crop_name','village']

class PointsLog(ModelSerializer):
	farmerField=FarmerFieldsLog(read_only=True)
	class Meta:
		model = Point
		fields = ['pk','latitude','longitude','farmerField']




