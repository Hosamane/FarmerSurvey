
from __future__ import unicode_literals
from django.db import models
from decimal import Decimal
from django.contrib.auth.models import User


# Create your models here.
class Institution(models.Model):
	institution_name=models.CharField(max_length=100)
	address=models.CharField(max_length=100)
	phone_no= models.CharField(max_length=20,null='true')
	latitude = models.DecimalField(max_digits=30,decimal_places=20)
	longitude = models.DecimalField(max_digits=30,decimal_places=20)

	def __unicode__(self):
		return "%s"%(self.institution_name)

		
class Member(models.Model):
	fcm_token = models.TextField(null=True,blank=True)
	profile = models.OneToOneField(User, related_name='member')
	name = models.CharField(max_length=100)
	address = models.TextField()
	phonenumber = models.CharField(max_length=20,null=True)
	is_preferred = models.BooleanField(default=False)
	institution = models.ManyToManyField(Institution)
	def __unicode__(self):
		return "%s"%(self.name)

class Crop(models.Model):
	crop_name=models.CharField(max_length=100)
	def __unicode__(self):
		return "%s"%(self.crop_name)

class Water_source(models.Model):
	water_source =models. CharField(max_length=100)

	def __unicode__(self):
		return "%s"%(self.water_source)

class Village(models.Model):
	village = models.CharField(max_length=100)
	institution = models.ForeignKey(Institution)
	def __unicode__(self):
		return "%s"%(self.village)


class FarmerDetail(models.Model):
	# profile = models.OneToOneField(User, related_name = 'user')
	name = models.CharField(max_length=100,unique='true')
	mobile_num = models.CharField(max_length=20,null='true')
	district = models.CharField(max_length=100)
	state = models.CharField(max_length=100,null = True)
	# id_proof=models.CharField(max_length=100)
	farmer_code=models.CharField(max_length=100,null=True)
	no_of_fields=models.IntegerField(null=True,default = 0)
	village=models.ForeignKey(Village)
	institution = models.ForeignKey(Institution)
	def __unicode__(self):
		return "%s"%(self.name)	


class FarmerField(models.Model):

	acars=models.DecimalField(max_digits=30,decimal_places=3)
	town=models.CharField(max_length=100)
	water_source =models.ForeignKey(Water_source)
	farmerDetails=models.ForeignKey(FarmerDetail)
	crop_name=models.ForeignKey(Crop)
	distance_from_institution=models.DecimalField(max_digits=20,decimal_places=10)
	village=models.ForeignKey(Village)

	def __unicode__(self):
		return "%s %s %s %s  %s %s"%(self.farmerDetails.name,self.acars,self.town,self.water_source,self.crop_name,self.village)

class Point(models.Model):
	latitude = models.DecimalField(max_digits=30,decimal_places=20)
	longitude = models.DecimalField(max_digits=30,decimal_places=20)
	farmerField=models.ForeignKey(FarmerField)

	def __unicode__(self):
		return "%s--%s"%(self.latitude,self.longitude)





	

