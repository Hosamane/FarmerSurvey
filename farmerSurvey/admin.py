# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from  models import *
# Register your models here.
admin.site.register(Institution)
admin.site.register(Member)
admin.site.register(FarmerDetail)
admin.site.register(Water_source)
admin.site.register(Point)
admin.site.register(Crop)
admin.site.register(FarmerField)
admin.site.register(Village)